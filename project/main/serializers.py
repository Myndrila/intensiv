from rest_framework import serializers
from .models import Intensives, Stages, Groups

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Groups
        fields = '__all__'

class StageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stages
        fields = '__all__'

class IntensiveSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)

    class Meta:
        model = Intensives
        fields = '__all__'