from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.views import View
from django.views.generic import DetailView
from rest_framework import generics

from .models import CustomUser, Intensives, Stages, Groups, Tasks, Responses

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout

from .form import LoginForm, IntensiveForm, TaskForm, ResponseForm
from .serializers import IntensiveSerializer, StageSerializer, GroupSerializer

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('account')
            else:
                form.add_error(None, 'Неверные учетные данные')
    else:
        form = LoginForm()

    return render(request, 'main/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('main')


def index(request):
    return render(request, 'main/index.html')


def account(request):
    return render(request, 'main/account.html')


class IntensiveCreateView(generics.CreateAPIView):
    queryset = Intensives.objects.all()
    serializer_class = IntensiveSerializer

@login_required
def create_intensive(request):
    if request.method == 'POST':
        form = IntensiveForm(request.POST)
        if form.is_valid():
            intensive = form.save(commit=False)
            intensive.owner = request.user
            intensive.save()
            return redirect('account')
    else:
        form = IntensiveForm()

    return render(request, 'main/create_intensive.html', {'form': form})


@login_required
def account(request):
    intensives = Intensives.objects.filter(owner=request.user)
    return render(request, 'main/account.html', {'intensives': intensives})


class IntensiveDetailView(View):
    template_name = 'main/intensive_detail.html'

    def get(self, request, pk):
        intensive = get_object_or_404(Intensives, pk=pk)
        tasks = Tasks.objects.filter(intensive=intensive)

        return render(request, 'main/intensive_detail.html', {'intensive': intensive, 'tasks': tasks})


class TasksCreateView(View):
    def get(self, request, intensive_id):
        intensive = get_object_or_404(Intensives, pk=intensive_id)
        form = TaskForm()
        return render(request, 'main/create_task.html', {'form': form, 'intensive': intensive})

    def post(self, request, intensive_id):
        intensive = get_object_or_404(Intensives, pk=intensive_id)
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.intensive = intensive
            task.save()
            return redirect('intensive-detail', pk=intensive_id)
        return render(request, 'main/create_task.html', {'form': form, 'intensive': intensive})


class TaskResponseView(View):
    def get(self, request, task_id):
        task = get_object_or_404(Tasks, pk=task_id)
        response_form = ResponseForm()
        response = Responses.objects.filter(student=request.user, task=task).first()

        if response:
            response_form = ResponseForm(instance=response)

        return render(request, 'main/task_detail.html',
                      {'response_form': response_form, 'task': task, 'response': response})

    def post(self, request, task_id):
        task = get_object_or_404(Tasks, pk=task_id)
        response = Responses.objects.filter(student=request.user, task=task).first()
        response_form = ResponseForm(request.POST, instance=response)

        if response_form.is_valid():
            response = response_form.save(commit=False)
            response.student = request.user
            response.task = task
            response.save()

            return redirect('task-detail', pk=task_id)

        return render(request, 'main/task_detail.html',
                      {'response_form': response_form, 'task': task, 'response': response})


class StageCreateView(generics.CreateAPIView):
    queryset = Stages.objects.all()
    serializer_class = StageSerializer


class TaskDetailView(DetailView):
    model = Tasks
    template_name = 'main/task_detail.html'
    context_object_name = 'task'
    def get(self, request, pk):
        task = get_object_or_404(Tasks, pk=pk)
        response = Responses.objects.filter(student=request.user, task=task).first()
        return render(request, 'main/task_detail.html', {'task': task, 'response': response})