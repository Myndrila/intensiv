from django.contrib import admin
from .models import Accounts, Roles, Profiles, Universities, Groups, CustomUser, Directions, Institutes, Intensives, \
    Tasks, Stages, Responses, TaskTypes

admin.site.register(Institutes)
admin.site.register(Directions)
admin.site.register(Profiles)
admin.site.register(Roles)
admin.site.register(Universities)
admin.site.register(Groups)
admin.site.register(CustomUser)
admin.site.register(Intensives)
admin.site.register(Stages)
admin.site.register(Tasks)
admin.site.register(Responses)
admin.site.register(TaskTypes)


