# import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')
# import django
# django.setup()

from django.test import TestCase, Client
from django.urls import reverse

from intensiv.project.main.models import Tasks, CustomUser


class TestTaskResponseView(TestCase):
    def setUp(self):
        self.user = CustomUser.objects.create(username='testuser', password='testpassword')
        self.task = Tasks.objects.create(name='Test Task', description='Test Description')

    def test_get_request(self):
        client = Client()
        response = client.get(reverse('task-response', args=[self.task.pk]))

        # self.assertEqual(response.status_code, 200)
