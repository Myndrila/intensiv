from django.contrib.auth.models import AbstractUser, Permission
from django.db import models


class Profiles(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Профиль работы'
        verbose_name_plural = 'Профили'


class Roles(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'


class Institutes(models.Model):
    name = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Институты'
        verbose_name_plural = 'Институт'


class Universities(models.Model):
    name = models.CharField(max_length=255)
    institute = models.ForeignKey(Institutes, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Университет'
        verbose_name_plural = 'Университеты'


class Directions(models.Model):
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Направление подготовки'
        verbose_name_plural = 'Направления подготовки'


class Groups(models.Model):
    name = models.CharField(max_length=255)
    direction = models.ForeignKey(Directions, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'


class CustomUser(AbstractUser):
    last_name = models.CharField(max_length=30, blank=True, null=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    middle_name = models.CharField(max_length=30, blank=True, null=True)
    year = models.PositiveIntegerField(blank=True, null=True)
    groups = models.ForeignKey(Groups, on_delete=models.SET_NULL, blank=True, null=True)
    roles = models.ForeignKey(Roles, on_delete=models.SET_NULL, blank=True,  null=True)
    institute = models.ForeignKey(Institutes, on_delete=models.SET_NULL, blank=True, null=True)
    profiles = models.ForeignKey(Profiles, on_delete=models.SET_NULL, blank=True,  null=True)
    listed = models.BooleanField(default=True, null=True)
    universities = models.ForeignKey(Universities, on_delete=models.SET_NULL, blank=True, null=True)
    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name='user permissions',
        blank=True,
        related_name='custom_user_permissions'
    )

    def __str__(self):
        return f"{self.last_name} {self.first_name} {self.middle_name}"

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Stages(models.Model):
    name = models.CharField(max_length=255)
    # tasks = models.ManyToManyField(Tasks)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Этап интенсива'
        verbose_name_plural = 'Этапы интенсива'


class Intensives(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, blank=True, null=True)
    stages = models.ManyToManyField(Stages)
    groups = models.ManyToManyField(Groups)

    def __str__(self):
        return self.name if self.name else str(self.pk)


    class Meta:
        verbose_name = 'Интенсив'
        verbose_name_plural = 'Интенсивы'


class TaskTypes(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип задания'
        verbose_name_plural = 'Типы заданий'

class Tasks(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    deadline = models.DateTimeField()
    intensive = models.ForeignKey(Intensives, on_delete=models.SET_NULL, blank=True, null=True)
    type = models.ForeignKey(TaskTypes, on_delete=models.SET_NULL, blank=True, null=True)
    # response = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'


class Responses(models.Model):
    student = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    response_text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    mark = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.task} {self.student}"

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'


class Accounts(models.Model):
    pass


class MainMeta:
    app_label = 'project.main'