from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, re_path
from . import views
from .views import IntensiveDetailView, TaskDetailView, TasksCreateView, TaskResponseView

urlpatterns = [
    path('', views.index, name='main'),
    path('accounts/login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='user_logout'),
    path('login/account/', views.account, name='account'),
    path('api/v1/auth/', include('djoser.urls')),
    re_path(r'^auth/', include('djoser.urls.authtoken')),
    path('create_intensive/', views.create_intensive, name='create_intensive'),
    path('intensive/<int:pk>/', IntensiveDetailView.as_view(), name='intensive-detail'),
    path('create_task/<int:intensive_id>/', TasksCreateView.as_view(), name='create_task'),
    path('task/<int:pk>/', TaskDetailView.as_view(), name='task-detail'),
    path('task-response/<int:task_id>/', TaskResponseView.as_view(), name='task-response'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)