from django import forms

from .models import Intensives, Tasks, Responses


class LoginForm(forms.Form):
    username = forms.CharField(label='Логин')
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)


class IntensiveForm(forms.ModelForm):
    class Meta:
        model = Intensives
        fields = ['name', 'groups']
        widgets = {
            'owner': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'placeholder': 'Название интенсива', 'required': False}),
        }
        labels = {
            'name': 'Название интенсива',
            'groups': 'Выберите группы',
        }


class TaskForm(forms.ModelForm):
    class Meta:
        model = Tasks
        fields = ['name', 'description', 'deadline']

        widgets = {
            'deadline': forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
            'name': forms.TextInput(attrs={'required': False}),
            'description': forms.TextInput(attrs={'required': False}),
        }
        labels = {
            'name': 'Заголовок',
            'description': 'Описание',
            'deadline': 'Срок выполнения',
        }


class ResponseForm(forms.ModelForm):
    class Meta:
        model = Responses
        fields = ['response_text']

        labels = {
                'response_text': 'Ответ',
            }
